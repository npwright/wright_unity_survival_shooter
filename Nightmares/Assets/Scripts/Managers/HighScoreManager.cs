﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScoreManager : MonoBehaviour {

	public static int highScore;
	int score = 0;

	Text text;


	void Start (){
		highScore = PlayerPrefs.GetInt ("highScore", 0);
	}

	void Awake (){
		text = GetComponent <Text> ();
	}

	void Update (){
			Death ();
	}


	void Death (){
		score = ScoreManager.score;

		if (score > highScore) {
			highScore = score;
			PlayerPrefs.SetInt ("highScore", highScore);
		}
		text.text = "High Score: " + highScore;
	}
}

