﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;


public class PlayerHealth : MonoBehaviour
{
    public int startingHealth = 100;
    public int currentHealth;
	public Slider healthSlider;
	public Image healthFill;
	public Image healthLipFill;
    public Image damageImage;
    public AudioClip deathClip;
    public float flashSpeed = 5f;
    public Color flashColour = new Color(1f, 0f, 0f, 0.1f);
	public int startingShield = 0;
	public int currentShield;
	public Slider shieldSlider;
	public Image shieldFill;
	public Image shieldLipFill;
	public float lipValue = 0.03f;

    Animator anim;
    AudioSource playerAudio;
    PlayerMovement playerMovement;
    PlayerShooting playerShooting;
    public bool isDead;
    bool damaged;


    void Awake ()
    {
        anim = GetComponent <Animator> ();
        playerAudio = GetComponent <AudioSource> ();
        playerMovement = GetComponent <PlayerMovement> ();
        playerShooting = GetComponentInChildren <PlayerShooting> ();
        currentHealth = startingHealth;
		currentShield = startingShield;

		healthAndShield ();

    }


    void Update ()
    {
		healthAndShield ();

        if(damaged)
        {
            damageImage.color = flashColour;
        }
        else
        {
            damageImage.color = Color.Lerp (damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }
        damaged = false; 


    }

/*	public void Heal (int amount){

		currentHealth += amount;
		healthSlider.value = currentHealth;
		healthFill.fillAmount = currentHealth / 100f;
		healthLipFill.fillAmount = currentHealth / 100f + lipValue;
	} */


    public void TakeDamage (int amount)
    {
		damaged = true;

		if (currentShield != 0)
		{
			currentShield -= amount;

			playerAudio.Play ();
		}
		else
		{

        currentHealth -= amount;

        playerAudio.Play ();

        if(currentHealth <= 0 && !isDead)
        	{
            Death (); 
       		} 
		}
    }


    void Death ()
    {
        isDead = true;

        playerShooting.DisableEffects ();

        anim.SetTrigger ("Die");

        playerAudio.clip = deathClip;
        playerAudio.Play ();

        playerMovement.enabled = false;
        playerShooting.enabled = false;
    }


    public void RestartLevel ()
    {
        SceneManager.LoadScene (0);
    }

	void OnTriggerEnter(Collider other){
		print ("heal");
		if (other.gameObject.CompareTag ("Health Pickup")) {
			print ("health");
			currentHealth += 10;
			if (currentHealth > 100) {
				currentHealth = 100;
			}
		}
		if (other.gameObject.CompareTag ("Shield Pickup")) {
			print ("shield");
			currentShield += 10;
			if (currentShield > 100) {
				currentShield = 100;
			}
		}
	}

	void healthAndShield(){
		shieldFill.fillAmount = currentShield / 100f;
		shieldLipFill.fillAmount = currentShield / 100f + lipValue;
		healthFill.fillAmount = currentHealth / 100f;
		healthLipFill.fillAmount = currentHealth / 100f + lipValue;
	}
		

}
