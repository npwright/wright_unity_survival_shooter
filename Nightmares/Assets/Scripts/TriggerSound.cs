﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TriggerSound : MonoBehaviour {

	public AudioClip soundFX;
	private float runTimer;

	void Awake(){

	}
	void Update() {
		runTimer += Time.deltaTime;

		if (runTimer >= 8) {
			Destroy (gameObject);
		}
	}

	void OnTriggerEnter (Collider other){
		print ("outside");
		if (other.tag == "Player") {

			GetComponent<AudioSource> ().PlayOneShot (soundFX);

		}
	}

}
